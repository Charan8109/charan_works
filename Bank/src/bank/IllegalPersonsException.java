/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class IllegalPersonsException extends Exception {

    /**
     * Creates a new instance of <code>IllegalPersonsException</code> without
     * detail message.
     */
    public IllegalPersonsException() {
    }

    /**
     * Constructs an instance of <code>IllegalPersonsException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public IllegalPersonsException(String msg) {
        super(msg);
    }
}
