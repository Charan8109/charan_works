/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;



/**
 *
 * @author Saicharan Vaddadi
 */
public class SavingAccount extends AbstractBankAccount{
    
    
    private static final double MINIMUM_BALANCE=100;

    public SavingAccount(int accountNo, double balance, int routingNumber) {
        super(accountNo, balance, routingNumber);
    }
    
    @Override
    public double calcInterest(int ageOfAccount)
    {
        
            return ((INTEREST_RATE_ACCOUNT-0.25)*((double)ageOfAccount/12)*super.getBalance())/100;
        
        
    }
    
    @Override
    public double calcBalance(double amount,String transactionType,int ageOfAccount)throws LowBalanceException,TransferLimitException{
        if(amount<=0)
            {
                throw new TransferLimitException("invalid amount");
            }
        double x=getBalance();
        if("Deposit".equals(transactionType))
        {
            
            x=x+amount+calcInterest(ageOfAccount);
           
         
        }
        else if("Withdrawal".equals(transactionType))
                {
                     if(x<amount)
                     {
                     
                     throw new LowBalanceException("Exception: Withdrawal failed due to low balance in Saving account");
                     }
                     else
                     {
                
                     x=x-amount-calcTransactionFee(amount)+calcInterest(ageOfAccount);
                
                }
                }
        super.setBalance(x);
        return x;
    }
    
    
    @Override
    public double calcTransactionFee(double amount)throws TransferLimitException{
        double x=0.0;
        if(amount>1000){
            throw new TransferLimitException("Exception: Withdrawal failed due to Transaction limit.");
        }
        if(amount>=0 && amount<=15)
        {
           x= TRANSACTION_FEE+1;
        }
        else if(amount>15 && amount<=100)
        {
           x= TRANSACTION_FEE+12;
        }
        else if(amount>100 && amount<=1000)
        {
            x= TRANSACTION_FEE+60;
        }
        return x;
    }
}

