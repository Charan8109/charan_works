/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class IndividualSavingAccount extends SavingAccount{
    
    private Person person;

    public IndividualSavingAccount(int accountNo, double balance, int routingNumber,String fName,String lName,int age) {
        super(accountNo, balance, routingNumber);
        this.person = new Person(fName, lName, age);
        
    }

    @Override
    public String toString() {
        return  person+ " " +super.toString() ;
    }
  
}
