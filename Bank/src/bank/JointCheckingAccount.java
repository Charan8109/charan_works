/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class JointCheckingAccount extends CheckingAccount{
    
    private Person[] persons;
    private int personsCount;
    
    public JointCheckingAccount(int accountNo, double balance, int routingNumber, int noOfPersons) {
        super(accountNo, balance, routingNumber);
        persons=new Person[noOfPersons];
        
    }
    
    public void addPerson(Person person)throws IllegalPersonsException{
        if(persons.length<0){
            throw new IllegalPersonsException("The Total number of persons cannot be less than zero");
        }
        else
        {
            
            persons[personsCount]=person;
                 personsCount++;
        }
    }

    @Override
    public String toString() {
       String s="";
        for(int i=0;i<persons.length;i++)
        {
            s+="First Name= "+persons[i].getfName()+", Last Name= "+persons[i].getLName()+", Age= "+persons[i].getAge()+"\n";
        }
        return s+"--------------------------------------------------------\nJoint Checking Account Details:\n"+super.toString();
    }

    
}
    
    
