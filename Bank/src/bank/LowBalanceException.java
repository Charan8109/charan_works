/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class LowBalanceException extends Exception {

    /**
     * Creates a new instance of <code>LowBalanceException</code> without detail
     * message.
     */
    public LowBalanceException() {
    }

    /**
     * Constructs an instance of <code>LowBalanceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public LowBalanceException(String msg) {
        super(msg);
    }
}

