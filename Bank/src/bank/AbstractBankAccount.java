/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public abstract class AbstractBankAccount implements Account {
    
    private int accountNo;
    private double balance;
    private int routingNumber;

    public AbstractBankAccount(int accountNo, double balance, int routingNumber) {
        this.accountNo = accountNo;
        this.balance = balance;
        this.routingNumber = routingNumber;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public double getBalance() {
        return balance;
    }

    public int getRoutingNumber() {
        return routingNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "accountNo= "+getAccountNo()+", balance= $"+getBalance()+", " + "routingNumber= "+getRoutingNumber();
    }
    @Override
    public abstract double calcInterest(int ageOfAccount);
  
          
    @Override
    public abstract double calcTransactionFee(double amount) throws TransferLimitException;
    
    public double calcBalance(double amount,String transactionType,int ageOfAccount) throws LowBalanceException,TransferLimitException
    {
       double x=getBalance();
            if(amount<=0)
            {
                throw new TransferLimitException("Exception: The amount cannot be negative or Zero");
            }
        
        if("Deposit".equals(transactionType))
        {
            
            x=x+amount+calcInterest(ageOfAccount);
            setBalance(x);
        
         
        }
        else if("Withdrawal".equals(transactionType))
                {
                     if(x<amount)
                     {
                     
                     throw new LowBalanceException("Exception: Withdrawal failed due to low balance in Saving account");
                     }
                     else
                     {
                
                     x=x-amount-calcTransactionFee(amount)+calcInterest(ageOfAccount);
            		setBalance(x);
                
                }
                }
        return x;
    }
    
}

