/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;


/**
 *
 * @author Saicharan Vaddadi
 */
public class CheckingAccount extends AbstractBankAccount{

    public CheckingAccount(int accountNo, double balance, int routingNumber) {
        super(accountNo, balance, routingNumber);
    }
    
    @Override
    public double calcInterest(int ageOfAccount){
      
            return ((INTEREST_RATE_ACCOUNT-0.25)*((double)ageOfAccount/12)*super.getBalance())/100;
       
    }
    
    @Override
    public double calcTransactionFee(double amount) throws TransferLimitException{
         double x=0.0;
       if(amount>2000){
            throw new TransferLimitException("Exception: Withdrawal failed due to Transaction limit.");
        }
        if(amount>=0 && amount<=15)
        {
           x= TRANSACTION_FEE+3;
        }
        else if(amount>15 && amount<=100)
        {
           x= TRANSACTION_FEE+17;
        }
        else if(amount>100 && amount<=1000)
        {
            x= TRANSACTION_FEE+75;
        }else if(amount>1000 && amount<=2000)
        {
            x= TRANSACTION_FEE+250;
        }
        return x;
    }
    
}
