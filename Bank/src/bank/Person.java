/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Person {
    
    private String fName;
    private String LName;
    private int age;

    public Person(String fName, String LName, int age) {
        this.fName = fName;
        this.LName = LName;
        this.age = age;
    }

    public String getfName() {
        return fName;
    }

    public String getLName() {
        return LName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "First name= "+getfName()+", Last name= "+getLName()+", age= "+getAge();
    }
    
    
}

