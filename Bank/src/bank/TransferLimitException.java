/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Saicharan Vaddadi
 */
public class TransferLimitException extends Exception {

    /**
     * Creates a new instance of <code>TransferLimitException</code> without
     * detail message.
     */
    public TransferLimitException() {
    }

    /**
     * Constructs an instance of <code>TransferLimitException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public TransferLimitException(String msg) {
        super(msg);
    }
}

