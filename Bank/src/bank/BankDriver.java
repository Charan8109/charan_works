/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Saicharan Vaddadi
 */
public class BankDriver {
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException throws a file not found exception
     */
    public static void main(String[] args) throws FileNotFoundException {

        Scanner sc=new Scanner(new File("Account.txt"));
        
        while(sc.hasNextLine()){
            String name;
            String transactionType;
            String paymentType;
            String accountType=sc.nextLine();
            if("IndividualSavingAccount".equals(accountType))
            {
               int accountNumber=sc.nextInt();
               int balance=sc.nextInt();
               int routingNumber=sc.nextInt();
               String Fname=sc.next();
               String Lname=sc.next();
               int age=sc.nextInt();
               
               if(age<18)
               {
                   KidSavingAccount kidAccount=new KidSavingAccount(accountNumber,(double)balance,routingNumber,Fname,Lname,age);
                   System.out.println("--------------------------------------------------------");
                   
                   System.out.println(kidAccount.toString());
                   try{
                       System.out.println(String.format("Balance in account if amount of 500 is deposited and ageOfAccount is 12 months= $%.2f",kidAccount.calcBalance(500, "Deposit", 12)));
                   }
                   catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  try
                  {
                      kidAccount.calcBalance(15000, "Withdrawal", 3);
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  
               }
               if(age>18)
               {
                   IndividualSavingAccount indSavingAcc=new IndividualSavingAccount(accountNumber,(double)balance, routingNumber, Fname, Lname, age);
                   System.out.println("--------------------------------------------------------");
                   System.out.println("Individual Details:");
                   System.out.println(indSavingAcc.toString());
                  try{
                      System.out.println(String.format("Balance in account if amount of 1500 is deposited and ageOfAccount is 12 months= $%.2f",indSavingAcc.calcBalance(1500,"Deposit",12)));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  try{
                      System.out.println(indSavingAcc.calcBalance(10000,"Withdrawal",3));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  
                   
               }
            }
            if("FamilySavingAccount".equals(accountType))
            {
                int accountNumber=sc.nextInt();
               int balance=sc.nextInt();
               int routingNumber=sc.nextInt();
               int noOfpersons=sc.nextInt();
               int count=0;
               sc.nextLine();
               System.out.println("--------------------------------------------------------");
               
               FamilySavingAccount FSAObject=new FamilySavingAccount(accountNumber, balance, routingNumber, noOfpersons);
               while(count<noOfpersons)
               {
                   String Fname=sc.next();
                   String Lname=sc.next();
                   int age=sc.nextInt();
                   Person person=new Person(Fname,Lname,age);
                   try{
                       FSAObject.addPerson(person);
                   count++;}
                   catch(IllegalPersonsException e)
                   {
                       System.out.println(e.getMessage());
                   }
                   
               }
                System.out.println("Person Details for Family Account:\n");
                System.out.println(FSAObject.toString());
                try{
                      System.out.println(String.format("Balance in account if amount of 5000 is Deposit and ageOfAccount is 5 months= $%.2f",FSAObject.calcBalance(5000,"Deposit",5)));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  try{
                      System.out.println(String.format("Balance in account if amount of 500 is withdraw and ageOfAccount is 10 months= $%.2f",FSAObject.calcBalance(500,"Withdrawal",10)));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  
                   
                
            }
            if("JointCheckingAccount".equals(accountType))
            {
               int accountNumber=sc.nextInt();
               int balance=sc.nextInt();
               int routingNumber=sc.nextInt();
               int noOfpersons=sc.nextInt();
               int count=0;
               System.out.println("--------------------------------------------------------");
               JointCheckingAccount  JCAObject=new JointCheckingAccount(accountNumber, balance, routingNumber, noOfpersons);
              
               while(count<noOfpersons)
               {
                   
                   String Fname=sc.next();
                   String Lname=sc.next();
                   int age=sc.nextInt();
                   Person person=new Person(Fname, Lname, age);
                   try{
                    JCAObject.addPerson(person);
                    count++;
                    
                    
                }   
                catch (IllegalPersonsException e) {
                    System.out.println(e.getMessage());
                }
               }
                System.out.println("Person Details for Joint Account Details:\n");
                System.out.println(JCAObject.toString());
                
                try{
                      System.out.println(String.format("Balance in account if amount of 5000 is Deposit and ageOfAccount is 5 months= $%.2f",JCAObject.calcBalance(5000,"Deposit",5)));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
                  try{
                      System.out.println(String.format("Balance in account if amount of 500 is withdraw and ageOfAccount is 10 months= $%.2f",JCAObject.calcBalance(500,"Withdrawal",10)));
                  }
                  catch(LowBalanceException | TransferLimitException e)
                           {
                               System.out.println(e.getMessage());
                           }
            }
        }
    }
}
