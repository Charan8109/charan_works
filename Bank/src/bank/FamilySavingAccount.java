/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Saicharan Vaddadi
 */
public class FamilySavingAccount extends SavingAccount{
    
    private ArrayList<Person> persons;
    private int personsCount;

    public FamilySavingAccount(int accountNo, double balance, int routingNumber,int noOfPersons) {
        super(accountNo, balance, routingNumber);
        persons= new ArrayList<>(noOfPersons);
    }
    
    public void addPerson(Person person) throws IllegalPersonsException{
        if(persons.size()<0){
            throw new IllegalPersonsException("Array size cannot be less than Zero");
        }
        persons.add(person);
        personsCount++;
    }

    @Override
    public String toString() {
        String s="";
        for(int i=0;i<persons.size();i++)
        {
            s+="First Name= "+persons.get(i).getfName()+", Last Name= "+persons.get(i).getLName()+", Age= "+persons.get(i).getAge()+"\n";
        }
        return s+"--------------------------------------------------------\nFamily Account Details:\n"+super.toString();

    }
    
}
