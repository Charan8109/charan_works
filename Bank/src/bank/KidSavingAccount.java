/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;



/**
 *
 * @author Saicharan Vaddadi
 */
public class KidSavingAccount extends IndividualSavingAccount {
    
    private static final double MINIMUM_BALANCE=50;

    public KidSavingAccount(int accountNo, double balance, int routingNumber, String fName, String lName, int age) {
        super(accountNo, balance, routingNumber, fName, lName, age);
    }
    
    @Override
    public double calcInterest(int ageOfAccount)
    {
        
            return (INTEREST_RATE_ACCOUNT*(ageOfAccount/12)*super.getBalance())/100;
        
        
    }
    
    @Override
    public double calcBalance(double amount,String transactionType,int ageOfAccount)throws LowBalanceException,TransferLimitException{
        
         if(amount<=0)
            {
                throw new TransferLimitException("Exception: Transaction amount cannot be negative or Zero");
            }
        double x=super.getBalance();
        if("Deposit".equals(transactionType))
        {
            
            x=x+amount+(calcInterest(ageOfAccount));
        
         
        }
        else if("Withdrawal".equals(transactionType))
                {
                     if(super.getBalance()>amount)
                     {
                     x=super.getBalance()-amount-calcTransactionFee(amount)+(calcInterest(ageOfAccount));
                     }else
                     {    
                     throw new LowBalanceException("Exception: Withdrawal failed due to low balance in Saving account");
                }
                     
                
                }
        
        return x;
    }
    
    @Override
    public double calcTransactionFee(double amount)throws TransferLimitException{
        if(amount>100){
            throw new TransferLimitException("Exception: Withdrawal failed due to Transaction limit.");
        }
       
        return TRANSACTION_FEE+1;
        
    }

    @Override
    public String toString() {
        return "Kid Details:\n"+super.toString();
    }
    
    
}
