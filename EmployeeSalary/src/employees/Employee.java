/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employees;

/**
 *This class initialize and declares the data of employees
 * @author s525816
 */
public class Employee {
   private String firstName,lastName,phoneNumber,address;
   private int employeeID;


/**
 * Constructor with arguments
 * @param firstName-maintains first name of an employee
 * @param lastName-maintains last name of an employee
 * @param employeeID-maintains ID of an employee
 * @param phoneNumber-maintains phone number of an employee
 * @param address-maintains address of an employee 
 */
    public Employee(String firstName, String lastName, int employeeID, String phoneNumber, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeID = employeeID;
        this.phoneNumber = phoneNumber;
        this.address = address;
        
    }
    /**
     * no argument constructor assigns no values.The default values would be stored in respective variables(i.e. 'null' for String variables, '0' for integer variables
     */
    public Employee()
    {  
        
    }
    /**
     *  gets the first name of employee
     * @return firstName-returns first name of the employee
     */
    public String getFirstName() 
    {
        return firstName;
    }
/**
 * gets the last name of employee
 * @return lastName -returns last name of the employee
 */
    public String getLastName() {
        return lastName;
    }
    /**
     * gets employeeID of the employee
     * @return employeeID-returns ID  of the employee
     */
    public int getEmployeeID() {
        return employeeID;
    }
    /**
     * gets phone number of the employee
     * @return phoneNumber-returns phone number of the employee 
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
/**
 * gets address of the employee
 * @return address-returns address of the employee
 */
    public String getAddress() {
        return address;
    }
/**
 * sets first name of the employee
 * @param firstName- it assigns first name of the employee
 */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
/**
 * sets last name of the employee
 * @param lastName- it assigns last name of the employee
 */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * sets employeeID of the employee
     * @param employeeID- it assigns ID of the employee
     */
    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }
    /**
     * sets phone number of the employee
     * @param phoneNumber- it assigns phone number of the employee
     */

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
/**
 * sets address of the employee
 * @param address- it assigns address of the employee
 */
    public void setAddress(String address) {
        this.address = address;
    }
    
/**
 *  modifying the method toString() as needed
 * @return- returns desired string 
 */
    @Override
    public String toString(){
     
     return firstName+" "+lastName+" with employeeID: "+employeeID+", phone number: "+phoneNumber+" and address: "+address ;
    }
    
}