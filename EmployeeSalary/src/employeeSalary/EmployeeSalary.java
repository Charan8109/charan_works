/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeeSalary;

/**
 * This class declare and initialize the factors of employee salary and computes the monthly salary, monthly insurance, annual gross salary and annual net pay.
 * @author s525816
 */
public class EmployeeSalary {
    
    private double hourlyRate,insuranceRate, taxRate, bonus;
    private int Hours=40;
/**
 * This constructor assigns parameter values to respective variables
 * @param hourlyRate , it maintains hourly rate of the employee
 * @param insuranceRate, it maintains insurance rate of the employee 
 * @param taxRate, it maintains tax rate of the employee 
 * @param bonus, it maintains bonus to the employee, once in a year
 */
    public EmployeeSalary(double hourlyRate, double insuranceRate, double taxRate, double bonus) {
        this.hourlyRate = hourlyRate;
        this.insuranceRate = insuranceRate;
        this.taxRate = taxRate;
        this.bonus = bonus;
    }
/**
 * no argument constructor assigns no values.The default values would be stored in respective variables(i.e. 'null' for String variables, '0' for integer variables
 */
    public EmployeeSalary() 
    {
        
    }
/**
 * gets hourly rate of the employee
 * @return hourlyRate- returns hourly pay of the employees salary
 */
    public double getHourlyRate() {
        return hourlyRate;
    }
/**
 * gets insurance rate of the employee
 * @return insuranceRate- returns insurance rate of the employee salary
 */
    public double getInsuranceRate() {
        return insuranceRate;
    }
/**
 * gets the tax rate of employee
 * @return taxRate, returns tax rate of the employee salary
 */
    public double getTaxRate() {
        return taxRate;
    }
/**
 * gets bonus amount of the employee
 * @return bonus- returns bonus amount of the employee salary
 */
    public double getBonus() {
        return bonus;
    }
/**
 * gets number of working hours per week
* @return Hours- returns number of working hours per week
 */
    public int getHours() {
        return Hours;
    }
/**
 * sets hourly pay rate of the employee
 * @param hourlyRate- it assigns hourly pay rate of the employee 
 */
    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
/**
 *sets insurance rate of the employee 
 * @param insuranceRate- it assigns insurance rate of the employee  
 */
    public void setInsuranceRate(double insuranceRate) {
        this.insuranceRate = insuranceRate;
    }
/**
 * sets tax rate of the employee
 * @param taxRate it assigns tax rate for employee 
 */
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }
/**
 * sets bonus amount of the employee
 * @param bonus- it assigns bonus amount of the employee 
 */
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
/**
 *sets working hours per week of the employee 
 * @param Hours- it assigns working hours per week of the employee 
 */
    public void setHours(int Hours) {
        this.Hours = Hours;
    }
/**
 * This method computes the monthly salary of an employee.
 * @return monthly salary, returns the employee salary
 */  
    public double monthlySalary(){
    return 4*hourlyRate*Hours;
    
    }
/**
 * This method computes the monthly insurance of an employee.
 * @return monthly insurance, returns the monthly insurance of an employee.
 */   
    public double monthlyInsurance(){
    return 4*hourlyRate*Hours*insuranceRate/100;
    
    }
/**
 * This method computes the annual gross salary of an employee.
 * @return annual gross salary- returns the annual gross salary of an employee.
 */   
    public double annualGrossSalary(){
    
    return 12*4*hourlyRate*Hours + bonus;
    }
/**
 * This method computes the annual net pay of an employee.
 * @return annual net pay- returns the annual gross salary of an employee.
 */   
    public double annualNetPay(){
    return (12*4*hourlyRate*Hours+bonus)-(((12*4*hourlyRate*Hours+bonus)*taxRate) /100)-( (12*4*hourlyRate*Hours*insuranceRate)/100);
    }

    /**
     * modifying the method toString() as needed
     * @return String-returns string value 
     */
    @Override
    public String toString() {
    return  "Hourly pay rate: $" + hourlyRate + ", insurance rate: " + insuranceRate + "%, tax: " + taxRate + "%, annual bonus: $" + bonus + ", Hours per week: " + Hours ;
    }
   
}