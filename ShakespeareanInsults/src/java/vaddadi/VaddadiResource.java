/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaddadi;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Saicharan Vaddadi
 */
@Path("vaddadi")
public class VaddadiResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of VaddadiResource
     */
    public VaddadiResource() {
    }

    /**
     * Retrieves representation of an instance of vaddadi.VaddadiResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        List<String> aj1 = new ArrayList<String>();
        aj1.add("puking");
        aj1.add("puny");
        aj1.add("qualling");
        aj1.add("rank");
        aj1.add("saucy");
        aj1.add("reeky");

        
        List<String> aj2 = new ArrayList<String>();
        aj2.add("knotty-pated");
        aj2.add("milk-livered");
        aj2.add("motley-minded");
        aj2.add("onion-eyed");
        aj2.add("spur-galled");
        aj2.add("plume-plucked");
        
        List<String> noun = new ArrayList<String>();
        noun.add("mammet");
        noun.add("measle");
        noun.add("coxcomb");
        noun.add("miscreant");
        noun.add("maggot-pie");
        noun.add("minnow");
        Random rand = new Random();
        
        JsonObject printres = Json.createObjectBuilder()
                .add("a",aj1.get(rand.nextInt(aj1.size())))
                .add("b",aj2.get(rand.nextInt(aj2.size())))
                .add("c",noun.get(rand.nextInt(noun.size()))).build();
        StringWriter strWriter = new StringWriter();
        JsonWriter jsonwriter = Json.createWriter(strWriter);
        jsonwriter.writeObject(printres);
        jsonwriter.close();
        return strWriter.toString();
    }

    /**
     * PUT method for updating or creating an instance of VaddadiResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
