/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Rhombus extends Parallelogram {
    private double diagonal1;
    private double diagonal2;

    /**
     * no argument constructor
     *
     */
    public Rhombus() {
        this(0.0,0.0);
    }

    /**constructor with two arguments
     *
     * @param diagonal1-diagonal1 is the parameter passed while generating object
     * @param diagonal2-diagonal2 is the parameter passed while generating object
     */
    public Rhombus(double diagonal1, double diagonal2) {
       
        this.diagonal1 = diagonal1;
        this.diagonal2 = diagonal2;
    }

    /**
     *this method sets the diagonal1 value by taking the diagonal1 as argument
     * @param diagonal1-diagonal1 is the parameter passed while generating object
     */
    public void setDiagonal1(double diagonal1) {
        this.diagonal1 = diagonal1;
    }

    /**
     *this method sets the diagonal2 value by taking the diagonal2 as argument 
     * @param diagonal2-diagonal2 is the parameter passed while generating object
     */
    public void setDiagonal2(double diagonal2) {
        this.diagonal2 = diagonal2;
    }
    
    /**
     *this method calculates the perimeter of rhombus by overriding the calculatePerimeter method in the parallelogram class
     * @return-returns the area of rhombus
     */
    @Override
    public double calculateArea()
    {
    return (this.diagonal1*this.diagonal2)/2; 
    }

    /**
     *this method calculates the perimeter value by overriding the calculatePerimeter method in the parallelogram class
        * @return-returns the perimeter of rhombus
     */
    @Override
    public double calculatePerimeter()
   {
   
  return 2*(Math.sqrt((this.diagonal1*this.diagonal1)+(this.diagonal2*this.diagonal2)));
                    
                    }
  /**
    this method returns the values of the object by overriding the toString() method of the Parallelogram class
     *@return-returns the desired string
    */
    @Override
    public String toString() {

    return String.format("\nDiagonal1 = %.2f\nDiagonal2 = %.2f\nArea = %.2f\nPerimeter = %.2f",diagonal1,diagonal2,calculateArea(),calculatePerimeter());
    }



}
