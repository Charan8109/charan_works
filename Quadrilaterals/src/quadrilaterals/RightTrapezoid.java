/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class RightTrapezoid extends Trapezoid{

    /**
     *constructor with no arguments.
     */
    public RightTrapezoid() {
    super();
    }

    /**constructor with four arguments 
     *
     * @param base1-base1 is the parameter passed while generating object 
     * @param base2-base2 is the parameter passed while generating object 
     * @param side1-side1 is the parameter passed while generating object 
     * @param side2-side2 is the parameter passed while generating object
     */
    public RightTrapezoid(double base1, double base2, double side1, double side2) {
        super(base1, base2, side1, side2, side2);
    }
    
    /**
     *this method sets the side1 value by overriding the setSide1 method in the Trapezoid class
     * 
     * @param side1-is the parameter passed while generating object
     */
@Override    
    public void setSide1(double side1)
    {
    super.setHeight(side1);
    super.setSide1(side1);
    }
      /**
    this method returns the values of the object by overriding the toString() method of the Trapezoid class
     *@return-returns the desired string
    */

    @Override
    public String toString() {
        return super.toString();
    }

}

