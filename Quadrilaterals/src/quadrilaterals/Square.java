/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Square extends Rhombus{

    /**constructor with no arguments.
     *
     */
    public Square() {
        super();
    }

    /**constructor with one argument 
     *
     * @param diagonal1-diagonal1 value is the parameter passed while generating object
     */
    public Square(double diagonal1) {
        super(diagonal1, diagonal1);
    }
    
    /**
     *this method sets the diagonal values
     * @param diagonal1-is the parameter passed while generating object
     */
    @Override
    public void setDiagonal1(double diagonal1)
    {
    
    super.setDiagonal1(diagonal1);
    super.setDiagonal2(diagonal1);
    }

      /**
    this method returns the values of the object by overriding the toString() method of the Rhombus class
     *@return-returns the desired string
    */
    
    @Override
    public String toString() {
        return super.toString();
    }
    
    
    
    
}