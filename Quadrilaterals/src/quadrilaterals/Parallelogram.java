/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Parallelogram extends Quadrilateral {
    
   private double base;
   private double side;
   private double height;

    /**
     *no argument constructor
     */
    public Parallelogram() {
        this(0.0,0.0,0.0);
        
    }

    /**
     *constructor with three arguments
     * @param base-base is the parameter passed while generating object
     * @param side-side is the parameter passed while generating object
     * @param height-height is the parameter passed while generating object
     */
    public Parallelogram(double base, double side, double height) {
        this.base = base;
        this.side = side;
        this.height = height;
    }

    /**
    this method calculates the area of parallelogram by overriding the calculateArea method in the Quadrilateral class
     *@return-returns the area value
    */

    @Override
    public double calculateArea()
    {
 
        return base*height;
    }
    /**
    this method calculates the perimeter of parallelogram by overriding the calculatePerimeter method in the Quadrilateral class
     *@return-returns the perimeter value
    */
    @Override
public double calculatePerimeter()
{
    
return 2*(base+side);
}

    /**
    *this method returns the values of the generated object by overriding the toString() method of the Quadrilateral class
     *@return-returns the desired string
    */
    @Override
    public String toString() {
        
   return String.format("\nBase = %.2f\nSide = %.2f\nHeight = %.2f\n%s",base,side,height,super.toString());
    }


}
