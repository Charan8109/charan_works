/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Saicharan Vaddadi
 */
public class QuadrilateralDriver {
    /**the program execution starts from here
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException throws file not found exception
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
    Scanner scan = new Scanner(new File("quadrilateral.txt"));
    
    while(scan.hasNext())
    {
    String type = scan.nextLine();
        if("Trapezoid".equals(type))
        {
while(scan.hasNextDouble())  
{

double Base1 = scan.nextDouble();
double Base2 = scan.nextDouble();       
double Height = scan.nextDouble();
double Side1 = scan.nextDouble();
double Side2 = scan.nextDouble();
Quadrilateral t = new Trapezoid(Base1,Base2,Height,Side1,Side2);
        System.out.println("Trapezoid:"+t.toString());
System.out.println("____________________________________________________________");
}
        }
     if("Right Trapezoid".equals(type))
    {
        
while(scan.hasNextDouble())  
{
double Base1 = scan.nextDouble();
double Base2 = scan.nextDouble();       
double Side1 = scan.nextDouble();
double Side2 = scan.nextDouble();
    RightTrapezoid rt = new RightTrapezoid(Base1,Base2,Side1,Side2);
  //Trapezoid rt = new RightTrapezoid(Base1,Base2,Side1,Side2);
 //Changing object type will not change the output because of the late binding polymorphism
        System.out.println("Right Trapezoid:"+rt.toString());
System.out.println("____________________________________________________________");
}
    
    }
    
     if("Parallelogram".equals(type))
    {
         
while(scan.hasNextDouble())  
{
double Base = scan.nextDouble();       
double Side = scan.nextDouble();
double Height = scan.nextDouble();

    Quadrilateral p = new Parallelogram(Base,Side,Height);
        System.out.println("parallelogram:"+p);
System.out.println("____________________________________________________________");
}
    
    }
    if("Rhombus".equals(type))
    {
    
while(scan.hasNextDouble())  
{
double diagonal1 = scan.nextDouble();       
double diagonal2 = scan.nextDouble();
        Quadrilateral r = new Rhombus(diagonal1,diagonal2);
        System.out.println("Rhombus:"+r.toString());
System.out.println("____________________________________________________________");
}
    
    }
     if("Square".equals(type))
    {
    
while(scan.hasNextDouble())  
{
double diagonal = scan.nextDouble();       
        Square s = new Square(diagonal);
     // Rhombus s = new Square(diagonal);
     //Changing object type will not change the output because of the late binding polymorphism
        System.out.println("Square:"+s.toString());
System.out.println("____________________________________________________________");


}   
}
}  
}   
}


    
