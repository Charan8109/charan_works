/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Quadrilateral {
    
    /**this method calculates the area of quadrilateral
     *
     * @return- returns the value
     */
    public double calculateArea(){
        return 0.0;
    }
    
    /**this method calculates the perimeter of quadrilateral
     *
     * @return- returns the value
     */
    public double calculatePerimeter(){
        return 0.0;
    }
    /**
    *this method returns the values of the generated object by overriding the toString() method
     *@return-returns the desired string
    */
    
    @Override
    public String toString() {
        return String.format("Area = %.2f \nPerimeter = %.2f",calculateArea(),calculatePerimeter()) ;
    }
    
    

    
}
