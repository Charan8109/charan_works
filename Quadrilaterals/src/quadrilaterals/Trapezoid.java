/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrilaterals;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Trapezoid extends Quadrilateral
{
private double base1;
private double base2;
private double height;
private double side1;
private double side2;

    /**
     *constructor with no arguments 
     */
    public Trapezoid() {
        base1=0.0;
        base2=0.0;
        height=0.0;
        side1=0.0;
        side2=0.0;
        
        
    }

    /**constructor with five arguments
     *
     * @param base1-base1 is the parameter passed while generating object
     * @param base2-base2 is the parameter passed while generating object 
     * @param height-height is the parameter passed while generating object
     * @param side1-side1 is the parameter passed while generating object
     * @param side2-side2 is the parameter passed while generating object 
     */
    public Trapezoid(double base1, double base2, double height, double side1, double side2) {
        this.base1 = base1;
        this.base2 = base2;
        this.height = height;
        this.side1 = side1;
        this.side2 = side2;
    }
    /**
     *this method sets the side value by taking the argument
     * @param side1-side1 is the parameter passed while generating object
     */
    public void setSide1(double side1) {
        this.side1 = side1;
    }

    /**
     *this method sets the height value by taking the argument
     * @param height-height is the parameter passed while generating object
     */
    public void setHeight(double height) {
        this.height = height;
    }

   
    /**
     *this method calculates the area by overriding the calculateArea method in the Quadrilateral class
     * @return-returns the area
     */
    @Override
    public double calculateArea()
    {
    return (((base1+base2)*height)/2);
    
    }

    /**
     *this method calculates the perimeter by overriding the calculatePerimeter method in the Quadrilateral class
     * @return-returns the perimeter
     */
    @Override
public double calculatePerimeter()
{

return (base1+base2+side1+side2);
}
    /**
    this method returns the values of the object by overriding the toString() method of the Quadrilateral class
     *@return-returns the desired string
    */
    @Override
    public String toString() {
    return String.format("\nBase1 = %.2f \nBase2 = %.2f\nHeight = %.2f\nSide1 = %.2f\nSide2 = %.2f\n%s", base1,base2,height,side1,side2,super.toString());
    }
}
