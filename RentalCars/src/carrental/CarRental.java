/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrental;

/**
 *
 * @author VaddadiSaicharan
 */
public class CarRental {
    
    private double baseCarRentalAmount,weekEndRate,insuranceAmount;
    private int numberOfDays,age,startDay;
    /**
     * 
     * @param baseCarRentalAmount inputs the base car rental value
     * @param weekEndRate inputs the week end rate
     * @param numberOfDays inputs the number of days taken for rent
     * @param age inputs the age of the person who took rent
     * @param insuranceAmount inputs the insurance amount
     * @param startDay inputs the start day of the rent
     */

    public CarRental(double baseCarRentalAmount, double weekEndRate, int numberOfDays, int age, double insuranceAmount ,int startDay) {
        this.baseCarRentalAmount = baseCarRentalAmount;
        this.weekEndRate = weekEndRate;
        this.insuranceAmount = insuranceAmount;
        this.numberOfDays = numberOfDays;
        this.age = age;
        this.startDay = startDay;
    }
    /**
     * get returns BaseCarRentalAmount
     * @return BaseCarRentalAmount
     */

    public double getBaseCarRentalAmount() {
        return baseCarRentalAmount;
    }
    /**
     * get returns WeekEndRate
     * @return getWeekEndRate
     */

    public double getWeekEndRate() {
        return weekEndRate;
    }
/**
 * get returns getInsuranceAmount
 * @return getInsuranceAmount
 */
    public double getInsuranceAmount() {
        return insuranceAmount;
    }
/**
 * get returns NumberOfDay
 * @return getNumberOfDays
 */
    public int getNumberOfDays() {
        return numberOfDays;
    }
/**
 * get returns Age
 * @return getAge
 */
    public int getAge() {
        return age;
    }
/**
 * get returns startDay
 * @return getStartDay
 */
    public int getStartDay() {
        return startDay;
    }/**
     * calculates getInsuranceAmount
     * @param age inputs the age value
     * @return getInsuranceAmount returns the insurance amount
     */
    
    
    public double getInsuranceAmount(int age){
       
        if (age<=25)
        return insuranceAmount+(0.1*insuranceAmount);
        else
        return insuranceAmount;
        
        
    }/**
     * calculates getDayRental
     * @param startDay inputs the start day of the rent
     * @return getDayRental returns the day rental
     */
    
    public double getDayRental(int startDay){
    if(startDay==0||startDay==6)
       return baseCarRentalAmount+((baseCarRentalAmount*weekEndRate)/100);
    else
        return baseCarRentalAmount;
    }/**
     * calculates getWeeklyRental
     * @param numberofWeeks inputs the number of weeks
     * @return getWeeklyRental returns the total weekly rental
     */
    
    public double getWeeklyRental(int numberofWeeks){
    double rent=0;
    int i;
    for(i=0;i<7;i++)
    {
    if(i==0||i==6){
    rent+=getDayRental(i)-((baseCarRentalAmount*weekEndRate)/100)/2;
    }
    else{
    rent+=getDayRental(i);
    }
    }
    rent= rent* numberofWeeks;
    return rent;      
    }       
            /**
             * calculates getTotalRental
             * @param startDay inputs the start day of the week
             * @param numberOfDays inputs the number of days taken for rent
             * @param age inputs the age
             * @return getTotalRental returns the total rental value
             */
    public double getTotalRental(int startDay, int numberOfDays, int age){
    double rent=0;
    int a=numberOfDays/7;
    int b=numberOfDays%7;
        rent+=getWeeklyRental(a);
        for(int i=0;i<b;i++)
        {
            if(startDay>6)
                startDay=startDay-7;
        rent+=getDayRental(startDay);
        startDay++;
        }
        rent+=getInsuranceAmount(age);
        return rent;
        }/**
         * returns expected output
         * @return String 
         */
    @Override
    public String toString()
    
    {
    
        return "The base car rental amount is: $"+baseCarRentalAmount+", weekend rate: "+weekEndRate+"%, number of days are: "+numberOfDays+", age of the driver is: "+age+", insurance amount is: $"+insuranceAmount+", starting day is: "+startDay;
    
    }    
    
}