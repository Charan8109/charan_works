/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stacksanddeques;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Saicharan Vaddadi
 */
public class RandomCharactersStack {
    
    
 public static void main(String[] args) throws FileNotFoundException   {
     
    AStack<Character> parentStack = new AStack<Character>();
    AStack<Character> resultantStack = new AStack<Character>();
    Scanner scan=new Scanner(new File("expressions.txt"));
    
    while(scan.hasNext()){
        String s=scan.nextLine();
        int p=0;
        int q=0;
        int i=0;
        int t=0;
        char u;
    
        AStack<Character> temp=new AStack<Character>();
        
        for(i=0;i<s.length();i++){
           
            parentStack.push(s.charAt(i));
            if(i==0)
            {
                p=(int)s.charAt(i);
            }
            else
            {
                q=(int)s.charAt(i);
                if(q<p)
                {
                    p=q;
                }
            }
        }
        char r=(char)p;
        do
        {
            
            while(!parentStack.isEmpty())
            {
                if(parentStack.peek().equals(r))
                {
                    resultantStack.push(parentStack.pop());
                }
                else
                {
                    temp.push(parentStack.pop());
                }
            }
            while(!temp.isEmpty())
            {
                if(t==0)
                {
                   r=(char)temp.peek();
                   t=(int)r;
                   parentStack.push(temp.pop());
                }
                else
                {
                    u=(char)temp.peek();
                    parentStack.push(temp.pop());
                    if((int)u<t)
                    {
                        t=(int)u;
                        r=u;
                    }
                    
                }
            }
            t=0;
        }while(!parentStack.isEmpty());         

        while(!resultantStack.isEmpty())
        {
            System.out.print(resultantStack.pop());
        }
        System.out.println("");
    }
    scan.close();
    }
}
