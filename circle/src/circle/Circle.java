/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circle;

/**
 *
 * @author S525816
 */
public class Circle {
    private double radius=1.0;
    private String color="red";

    public Circle() {
        radius=1.0;
        color="red";
    }
     public Circle(double radius){
         this.radius=radius;
     
     }
     public double getRadius(){
     return radius;
     }
      public String getColor(){
     return color;
     }
     public double getArea(double radius){
         return Math.PI*radius*radius;
     
     }
  
}
