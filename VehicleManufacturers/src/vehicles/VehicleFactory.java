/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author Saicharan Vaddadi
 */
public interface VehicleFactory {
    
    public final double STANDARD_MILEAGE=30.00;
    public abstract double calculateActualMileage();
    
}
