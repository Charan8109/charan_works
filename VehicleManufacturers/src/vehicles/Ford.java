/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;



import java.util.List;

/**
 *
 * @author saicharan vaddadi
 */
public class Ford extends AbstractVehicle implements VehicleFactory{
    
    private final double MILEAGE_FACTOR=0.75;

     /**
     *argument constructor
     * @param model passes mode1 parameter
     * @param engine passes engine parameter
     * @param transmission passes transmission parameter
     * @param body passes body parameter
     * @param doors passes doors parameter 
     * @param initialPrice passes initialPrice parameter
     */
    public Ford(String model, String engine, String transmission, String body, int doors, double initialPrice) {
        super(model, engine, transmission, body, doors, initialPrice);
    }

    /**
     *no-argument constructor
     */
    public Ford() {
    }
    /**
     * 
     * @param accessory passes accessory parameter
     * @return double, returns a double value which is the total cost
     */
    @Override
    public double calculateCost(List<String> accessory){
        double RemoteAccess=300,InteriorLightKit=450,GraphicsKit=399,LeatherSeats=750,AshCup=90,CargoOrganizer=152,totalCost=super.getInitialPrice();
        
       for(String a: accessory)
        {       
         if(null != a)
            switch (a) {
                case "Remote Access":
                    totalCost+=RemoteAccess;
                    break;
                case "Interior Light Kit":
                    totalCost+=InteriorLightKit;
                    break;
                case "Graphics Kit":
                    totalCost+=GraphicsKit;
                    break;
                case "Leather Seats":
                    totalCost+=LeatherSeats;
                    break;
                case "Ash Cup":
                    totalCost+=AshCup;
                    break;
                case "Cargo Organizer":
                    totalCost+=CargoOrganizer;
                    break;
                default:
                    break;
            }
        }
        return totalCost;      
    }
/**
 * 
 * @return String, returns the desired string
 */
    @Override
    public String toString() {
        return "Ford:\n"+super.toString();
    }

    /**
     *
     * Calculate actual milage 
     * @return, returns double value which is the actual mileage
     */
    @Override
    public double calculateActualMileage() {
        return MILEAGE_FACTOR*STANDARD_MILEAGE;    
    }
}
