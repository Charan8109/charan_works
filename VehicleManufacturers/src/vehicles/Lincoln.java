/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

import java.util.List;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Lincoln extends AbstractVehicle implements VehicleFactory{
    
    
    private final double MILEAGE_FACTOR=0.82;

     /**
     *argument constructor
     * @param model passes mode1 parameter
     * @param engine passes engine parameter
     * @param transmission passes transmission parameter
     * @param body passes body parameter
     * @param doors passes doors parameter 
     * @param initialPrice passes initialPrice parameter
     */
    public Lincoln(String model, String engine, String transmission, String body, int doors, double initialPrice) {
        super(model, engine, transmission, body, doors, initialPrice);
    }

    /**
     *no argument constructor
     */
    public Lincoln() {
    }
/**
 * 
 * @param accessory passes accessory parameter
 * @return double, returns a double value which is the total cost
 */
   
    @Override
    public double calculateCost(List<String> accessory) {
         double RemoteAccess=350,InteriorLightKit=400,GraphicsKit=699,LeatherSeats=710,AshCup=199,CargoOrganizer=172,totalCost=super.getInitialPrice(); 
       
         for(String a: accessory)
        {       
         if("Remote Access".equals(a))
           totalCost+=RemoteAccess;
        if("Interior Light Kit".equals(a))
           totalCost+=InteriorLightKit;
        if("Graphics Kit".equals(a))
           totalCost+=GraphicsKit;
        if("Leather Seats".equals(a))
           totalCost+=LeatherSeats;
        if("Ash Cup".equals(a))
           totalCost+=AshCup;
        if("Cargo Organizer".equals(a))
           totalCost+=CargoOrganizer;
        }
        return totalCost;
    }
    /**
     * 
     * @return String, returns the desired string
     */
    @Override
    public String toString() {
        return getAccessories()+"\nLincoln:\n"+super.toString();
    }

    /**
     *
     * @return double value which is the actual mileage
     */
    @Override
    public double calculateActualMileage() {
       return MILEAGE_FACTOR*STANDARD_MILEAGE;   
    }
 
}
