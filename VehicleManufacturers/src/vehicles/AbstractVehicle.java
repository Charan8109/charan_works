/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saicharan vaddadi
 */
public abstract class AbstractVehicle implements VehicleFactory {
    
    private String model,engine,transmission,body;
    private int doors;
    private double initialPrice;
    private List<String> accessories;

    /**
     *argument constructor
     * @param model passes mode1 parameter
     * @param engine passes engine parameter
     * @param transmission passes transmission parameter
     * @param body passes body parameter
     * @param doors passes doors parameter 
     * @param initialPrice passes initialPrice parameter
     */
    public AbstractVehicle(String model, String engine, String transmission, String body, int doors, double initialPrice) {
        this.model = model;
        this.engine = engine;
        this.transmission = transmission;
        this.body = body;
        this.doors = doors;
        this.initialPrice = initialPrice;
        accessories = new ArrayList();
    }

    /**
     *no-argument constructor
     */
    public AbstractVehicle() {
    }

    /**
     *
     * @param items passes items parameter
     * @return accessories, returns the list of accessories 
     */
    public List<String> addAccessories(String items){    
    
         String[] q=items.split(",");
        for(String q1 : q) {
            accessories.add(q1);
        }
        return accessories;
    }

    /**
     *
     * @return mode1, returns the values of mode
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @return engine, returns the value of engine 
     */
    public String getEngine() {
        return engine;
    }

    /**
     *
     * @return transmission, returns the value of transmission
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     *
     * @return body, returns the value of body
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @return doors, returns the value of doors
     */
    public int getDoors() {
        return doors;
    }

    /**
     *
     * @return price, returns the value of initial price
     */
    public double getInitialPrice() {
        return initialPrice;
    }

    /**
     *
     * @return accessories, returns the list of accessories
     */
    public List<String> getAccessories() {
        return accessories;
    }

     @Override
    public String toString() {
       return String.format("Model: %s\nEngine: %s\nBody: %s\nDoors: %d\nTransmission: %s\nInitial Price: $%.2f\nGiven Mileage: %.2f miles/gallon ",model,engine,body,doors,transmission,initialPrice,VehicleFactory.STANDARD_MILEAGE);
    }
    
    /**
     *
     * @param accessory passes accessory parameter
     * @return double, returns a double value
     */
    public abstract double calculateCost(List<String> accessory); 
    
}
