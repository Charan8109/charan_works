/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author saicharan vaddadi
 */
public class VehicleDriver {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException throws an exception
     */
    public static void main(String[] args) throws FileNotFoundException {
        try ( // TODO code application logic here
                Scanner scan = new Scanner(new File("vehicle.txt"))) {
            while (scan.hasNext())
            {
                String head=scan.nextLine();                
                if(head.equals("Ford"))
                {
                    
                    String model=scan.nextLine();
                    String engine=scan.nextLine();
                    String transmission=scan.nextLine();
                    String body=scan.nextLine();
                    int doors=scan.nextInt();
                    double initalPrice=scan.nextDouble();
                    
                    String st = scan.nextLine();
                    
                               
                    Ford f= new Ford(model,engine,transmission,body,doors,initalPrice);
                    f.addAccessories(st);
                   
                    System.out.println(f.toString());
                    System.out.println(String.format("Actual mileage: %.2f miles/gallon",f.calculateActualMileage()));
                    String S="";
                    for(int i=0;i<f.getAccessories().size();i++)
                    {
                        S=S.concat(f.getAccessories().get(i)+" ");
                    }
                    System.out.println("Accessories Added: "+S);
                    List<String> accessories= new ArrayList<>();
                    String x=st;
                    String[] q=x.split(",");
                    for (String q1 : q) {
                        accessories.add(q1);
                    }
                    System.out.println("Total Cost: $"+f.calculateCost(accessories));
                    System.out.println("--------------------------------------------------");
                    
                }
                if(head.equals("Lincoln"))
                {
                    
                    String model=scan.nextLine();
                    String engine=scan.nextLine();
                    String transmission=scan.nextLine();
                    String body=scan.nextLine();
                    int doors=scan.nextInt();
                    double initalPrice=scan.nextDouble();
                    
                    String st = scan.nextLine();
                    
                               
                    Lincoln l= new Lincoln(model,engine,transmission,body,doors,initalPrice);
                    l.addAccessories(st);
                    
                    System.out.println(l.toString());
                    System.out.println(String.format("Actual mileage: %.2f miles/gallon",l.calculateActualMileage()));
                    String S="";
                    for(int i=0;i<l.getAccessories().size();i++)
                    {
                        S=S.concat(l.getAccessories().get(i)+"  ");
                    }
                    System.out.println("Accessories Added:  "+S);
                    List<String> accessories= new ArrayList<>();
                    String x=st;
                    String[] q=x.split(",");
                    for (String q1 : q) {
                        accessories.add(q1);
                    }
                    System.out.println("Total Cost: $"+l.calculateCost(accessories));
                    System.out.println("--------------------------------------------------");
                }
            }
        }
    }
    
}
