/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project09;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saicharan Vaddadi
 */
@WebServlet(name = "CPS", urlPatterns = {"/CPS"})
public class CPS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         Canada can = new Canada();
        ArrayList<Province> sol = can.getProvinces();
        
        
        try (PrintWriter PW = response.getWriter()) {
            

            
        String pop = request.getParameter("population");
        String lan = request.getParameter("land");
        String wat = request.getParameter("water");
        long population = Long.parseLong(pop);
        long land = Long.parseLong(lan);
        long water = Long.parseLong(wat);
            
            try (JsonGenerator JG = Json.createGenerator(PW)) {
                JG.writeStartObject();
                    JG.writeStartArray("province");
                    for (Province cp : sol)  {
                      if (population < cp.getPopulation() && land < cp.getLandArea()&& water < cp.getWaterArea()){
                        JG.writeStartObject();
                        JG.write("name", cp.getName());
                        JG.write("population", cp.getPopulation());
                        JG.write("landArea", cp.getLandArea());
                        JG.write("waterArea", cp.getWaterArea());
                        JG.write("totalArea", cp.getTotalArea());
                        JG.write("officialLanguage", cp.getOfficialLanguage());
                        JG.writeEnd();
                    }
                    }
                    JG.writeEnd();
                JG.writeEnd();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
