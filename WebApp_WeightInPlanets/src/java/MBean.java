
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Saicharan Vaddadi
 */
@Named(value = "MBean")
@RequestScoped

public class MBean {

    private String name;
    private Double weight;
    private Double desired;
    private String planet;
    private boolean metric;

    double mercury = 0.0;
    double venus = 0.0;
    double earth = 0.0;
    double mars = 0.0;
    double jupiter = 0.0;
    double saturn = 0.0;
    double uranus = 0.0;
    double neptune = 0.0;
    double firstValue = 0.0;
    double PDesiredValue[];

    
    public MBean() {
    }

    public String getName() {
        return name;
    }

    public Double getWeight() {
        return weight;
    }

    public Double getDesired() {
        return desired;
    }

    public String getPlanet() {
        return planet;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public void setDesired(Double desired) {
        this.desired = desired;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    public boolean getMetric() {
        return metric;
    }

    public void setMetric(boolean metric) {
        this.metric = metric;
    }

    public double showWeight() {
        String planets[] = planet.split(" ");
        PDesiredValue = new double[planets.length];
        System.out.println(PDesiredValue[0]);
        

        for (int i = 0; i < planets.length; i++) {
            if ("Mercury".equals(planets[i])) {
                mercury = weight * 0.06;
                PDesiredValue[i] = mercury;
            }
            if ("Venus".equals(planets[i])) {
                venus = weight * 0.82;
                PDesiredValue[i] = venus;
            }
            if ("Earth".equals(planets[i])) {
                earth = weight * 1.0;
                PDesiredValue[i] = earth;
            }
            if ("Mars".equals(planets[i])) {
                mars = weight * 0.11;
                PDesiredValue[i] = mars;
            }
            if ("Jupiter".equals(planets[i])) {
                jupiter = weight * 317.8;
                PDesiredValue[i] = jupiter;
            }
            if ("Saturn".equals(planets[i])) {
                saturn = weight * 95.2;
                PDesiredValue[i] = saturn;
            }
            if ("Uranus".equals(planets[i])) {
                uranus = weight * 14.6;
                PDesiredValue[i] = uranus;
            }
            if ("Neptune".equals(planets[i])) {
                neptune = weight * 17.2;
                PDesiredValue[i] = neptune;
            }
        }
        
        double dist = Math.abs(PDesiredValue[0] - desired);
        int i = 0;
        for (int c = 1; c < PDesiredValue.length; c++) {
            double cdist = Math.abs(PDesiredValue[c] - desired);
            if (cdist < dist) {
                i = c;
                dist = cdist;
            }
        }
        double NearValue = PDesiredValue[i];
        name = planets[i];
       return NearValue;
    }

    

    public String metricvalue() {
        
       return metric?"Kgs":"Lbs";
       
    }

}