/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.Random;

/**
 *
 * @author VaddadiSaicharan
 */
public class StringManipulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Declaring and initialising string1,string2, string3,string4
        String string1="        First";  
        String string2=" Lab ";
        String string3="Computer";
        String string4=" Program     ";
        
        //Declaring and initialising string5 by concatinating string1,string2,string3,string4
        String string5=string1+string2+string3+string4;
        //Finding length of the concatenated string
        System.out.println("Length of the Concatenated string: "+string5.length());
        //trimming the spaces on both sides of the string5. declaring and initialising string6 by trimmed string5
        String string6=string5.trim();
        //Finding the length of the string6
        System.out.println("Length after removal of leading and trailing spaces: "+string6.length());
        //Finding the index of r in the word 'Program' of the string6
        System.out.println("Index of first r in Program: "+string6.substring(string6.indexOf("Program")).indexOf("r"));
        
        System.out.println("");
       //Finding the index of fun in fnunfunisfufunnufn
        System.out.println("Index of fun: "+"fnunfunisfufunnufn".indexOf("fun"));
        //Concatinating Programming with ' is and fun from fnunfunisfufunnufn'
        System.out.println("The resultant String is: "+"Programming ".concat("fnunfunisfufunnufn".substring("fnunfunisfufunnufn".indexOf("is"),"fnunfunisfufunnufn".indexOf("is")+"is".length()).concat(" ").concat("fnunfunisfufunnufn".substring("fnunfunisfufunnufn".indexOf("fun"),"fnunfunisfufunnufn".indexOf("fun")+"fun".length())))); 
        
        System.out.println("");
          
        //Declaring and initialising myValue
        double myValue=48.50;
        //Calculating ceiling and flooring value of square root of myValue
        System.out.println("The ceiling value of the square root of "+myValue+" is "+Math.ceil(Math.sqrt(myValue)));
        System.out.println("The flooring value of the square root of "+myValue+" is "+Math.floor(Math.sqrt(myValue)));
        
        System.out.println("");
        
        //Declaring and initialising myValue1,myValue2
        double myValue1=90,myValue2=20;
        
        System.out.print("the sine and tangent of first variable is "); 
        //Finding sine and tangent of myValue1
        System.out.print(Math.round(Math.sin(myValue1))); 
        System.out.print(" and ");
        System.out.println(Math.round(Math.tan(myValue1))); 
        System.out.print("the sine and tangent of second variable is ");
         //Finding sine and tangent of myValue2
        System.out.print(Math.round(Math.sin(myValue2)));
        System.out.print(" and ");
        System.out.println(Math.round(Math.tan(myValue2))); 
        
        System.out.println("");
         
        //Declaring and initialising myNumber1,myNumber2
        int myNumber1=3,myNumber2=5; 
        System.out.println(myNumber1+" raised to the power of "+myNumber2+" is "+Math.pow(myNumber1, myNumber2));
        
        System.out.println("");
        
        //finding the value of cosh(√(3^2+4(2)(2)+3)/2(4) ) 
        System.out.println("the ceiling value of the given expression is "+Math.ceil(Math.cosh(Math.sqrt(Math.addExact(Math.addExact(Math.multiplyExact(3, 3), Math.multiplyExact(Math.multiplyExact(4, 2), 2)), 3))/Math.multiplyExact(2, 4))));

        System.out.println("");
        
        //Generating five random numbers by using seed value
        Random R1= new Random(20L); 
         
        System.out.println("Case: When seed value is given"); 
        System.out.println("");
        
                    System.out.print("first random value: ");
                    System.out.println(R1.nextInt(100));
               
                    System.out.print("second random value: ");
                    System.out.println(R1.nextInt(100));
               
                    System.out.print("third random value: ");
                    System.out.println(R1.nextInt(100));
                 
                    System.out.print("fourth random value: ");
                    System.out.println(R1.nextInt(100));
                  
                    System.out.print("fifth random value: ");
                    System.out.println(R1.nextInt(100));
                  
            

        System.out.println("");
        System.out.println("When we repeat the execution, the values are not changing");
        System.out.println("");
         //Generating five random numbers without using seed value
        System.out.println("Case: When no seed value is given"); 
        System.out.println("");
        Random R2= new Random(); 
         
                    System.out.print("first random value: ");
                    System.out.println(R2.nextInt(1));
                  
                    System.out.print("second random value: ");
                    System.out.println(R2.nextInt(100));
                  
                    System.out.print("third random value: ");
                    System.out.println(R2.nextInt(100));
                  
                    System.out.print("fourth random value: ");
                    System.out.println(R2.nextInt(100));
                   
                    System.out.print("fifth random value: ");
                    System.out.println(R2.nextInt(100));
                   
   
        
        System.out.println("");
        System.out.println("When we repeat the execution, the values are changing.");
        System.out.println("We can understand this as: if we give seed value, random numbers generated by a particular formulae i.e. pseudorandom numbers are generating. Vice versa");
         } }
        