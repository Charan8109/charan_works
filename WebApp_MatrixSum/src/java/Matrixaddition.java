/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saicharan Vaddadi
 */
@WebServlet(urlPatterns = {"/sai"})
public class Matrixaddition extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String array1= request.getParameter("arrayA");
        String array2= request.getParameter("arrayB");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ARRAY_ADDITION</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h2> Addition Operation on 2D Arrays.</h2>"); 
            String A = array1;
            String B = array2;
            boolean key=true;
            String[] r1 = A.split(";");
            String[] r2 = B.split(";");
            String[] c1 = null;
            String[] c2 = null;
            if(r1.length != r2.length){
               out.println("<h2>ERROR</h2>");
               out.println("<h2>Arrays must be of equal size for addition.</h2>"); 
             
            }
            
            
            else{
               
            for(int i = 0;i<r1.length;i++){
                c1=r1[i].split(" ");
                c2=r2[i].split(" ");
                
                
                if(c1.length!=c2.length)
                {
                    key=false;
                    out.println("<h2>ERROR</h2>");
                    out.println("<h2>Arrays must be of equal size for addition.</h2>");
                    break;
                }
                
                
            }
             }
            if(key!=false){
            int[][] arrA = new int[r1.length][c1.length];
            int[][] arrB = new int[r2.length][c2.length];
            
            for(int i=0;i<r1.length;i++){
                c1=r1[i].split(" ");
                for(int j=0;j<c1.length;j++){
                    arrA[i][j]=Integer.parseInt(c1[j]);
                }
            }
            
            
            for(int i=0;i<r2.length;i++){
                c2=r2[i].split(" ");
                for(int j=0;j<c2.length;j++){
                    arrB[i][j]=Integer.parseInt(c2[j]);
                }
            }
            
            
            int[][] arrC = new int[r1.length][c1.length];
             
            for(int i=0;i<r1.length;i++){
                for(int j=0;j<c1.length;j++){
                    arrC[i][j] = arrA[i][j] + arrB[i][j];
                }
            }
            
           
            for(int i=0;i<r1.length;i++){
                for(int j=0;j<c1.length;j++){
                    
                    out.println(arrA[i][j] + " " );
                   
                }
                if(i!=(r1.length/2))
                out.print("&nbsp&nbsp&nbsp&nbsp");
                else
                out.print("&nbsp+&nbsp");  
                for(int j=0;j<c1.length;j++){
                   
                    out.print(arrB[i][j] + " " );
                   
                }
                 if(i!=(r1.length/2))
                out.print("&nbsp&nbsp&nbsp&nbsp");
                else
                out.print("&nbsp=&nbsp");
                for(int j=0;j<c1.length;j++){
                   
                    out.print(arrC[i][j] + " " );
                   
                }
                out.print("<br>");
            
            }
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
