-- Saicharan Vaddadi
-- Section 03


-- Q1. query to create a user with your first Name.
CREATE USER saicharan
IDENTIFIED BY saicharan;

-- Q2. query to grant appropriate permissions to this user.
GRANT DBA TO saicharan
WITH ADMIN OPTION;


-- Q5. Creating Department table with the Departmentno as primary key
CREATE TABLE Department (
DepartmentNo NUMBER(5),
DepartmentName VARCHAR2(45),
Description VARCHAR2 (100),
CONSTRAINT Department_Pk PRIMARY KEY (DepartmentNo)
);

-- Q6. Inserting data into the Department table.
INSERT ALL
INTO Department ( departmentno, departmentName, description)
VALUES (1, 'HE', 'Health')
INTO Department ( departmentno, departmentName, description)
VALUES (2, 'HR', 'Human Resources')
INTO Department (departmentno, departmentName, description)
VALUES (3, 'MIS', 'Media')
INTO Department (departmentno, departmentName, description)
VALUES (4, 'IL', 'Internal')
INTO Department (departmentno, departmentName, description)
VALUES (5, 'MGMT', 'Management')
SELECT * FROM dual;

-- Q7. Adding a constraint foriegn key on Employee table
Alter table Employee
ADD CONSTRAINT FK_Employee
FOREIGN KEY (departmentno)
REFERENCES Department (departmentno);

-- Q8. Details of the employees with given empno
SELECT *
FROM Employee
WHERE empno IN (85555038199, 96290411099, 10470430499);

-- Q9. List of distinct companies.
SELECT DISTINCT company
FROM employee;

-- Q10. Number of employees in each company.
SELECT company, COUNT(empno)
FROM employee
GROUP BY company;

-- Q11. Employee details of the Web developer and System engineer.
SELECT *
FROM employee
WHERE position IN ('Web Developer', 'System Engineer');

-- Q12. Employee names who are not from departments 2,3 or 4.
SELECT empname
FROM employee
WHERE departmentno NOT IN (2, 3, 4);

-- Q13. Total Salary.
SELECT SUM(salary)
FROM employee;

-- Q14. minimum, maximum and average salary in each department.
SELECT MIN(salary), MAX(salary), AVG(salary)
FROM employee
GROUP BY departmentno;

-- Q15. name and salary of highest paid employee.
SELECT empname, salary
FROM employee
WHERE salary = (SELECT MAX(salary) FROM employee);

-- Q16. cartesian product of two tables.
SELECT *
FROM employee, department;

-- Q17. employee names and description of their department.
SELECT empname, description
FROM employee, department
WHERE employee.departmentno = department.departmentno;

-- Q18. delete employee 'Paul, Upton I';
DELETE FROM Employee
WHERE TRIM(empname) = 'Paul, Upton I';

