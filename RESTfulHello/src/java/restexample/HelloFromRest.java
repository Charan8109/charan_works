/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restexample;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author s525816
 */
@Path("resthello")
public class HelloFromRest {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of HelloFromRest
     */
    public HelloFromRest() {
    }

    /**
     * Retrieves representation of an instance of restexample.HelloFromRest
     * @return an instance of java.lang.String
     */
   @GET
@Produces("text/html")
public String getHtml()
{
   return "<html><body><h1>Hello from REST</h1></body></html>";
}


    /**
     * PUT method for updating or creating an instance of HelloFromRest
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_HTML)
    public void putHtml(String content) {
    }
}
