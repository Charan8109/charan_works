package setsandmaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * TeamDriver 
 * @author Saicharan Vaddadi
 */
public class TeamDriver {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        HashSet<Team> hashTeams = new HashSet<Team>();

        TreeMap<Player, HashSet<Team>> treeMaps = new TreeMap<Player, HashSet<Team>>();

        Scanner sc = new Scanner(new File("teams.txt"));
        String PLName, PFName, TName, CName, Owner;
        double TPrice;        
        int Players;
        String[] Owners;
            while (sc.hasNext()) {
            PLName = sc.nextLine();
            PFName = sc.nextLine();
            TName = sc.nextLine();
            CName = sc.nextLine();
            TPrice = sc.nextDouble();
            Players = sc.nextInt();
            sc.nextLine();
            Owner = sc.nextLine();

            Owners = Owner.split(",");
            Player PL = new Player(PFName, PLName);
            Team TM = new Team(TName, CName, TPrice, Players, Owners);

            if (treeMaps.containsKey(PL) == false) {
                HashSet<Team> TM2 = new HashSet<Team>();
                TM2.add(TM);
                treeMaps.put(PL, TM2);

            } else if (treeMaps.get(PL) == null) {
                treeMaps.put(PL, hashTeams);
            } else {
                treeMaps.get(PL).add(TM);
            }
        }
            
        System.out.println("Print the key and value pair using for loop:");
        for (Map.Entry<Player, HashSet<Team>> entry : treeMaps.entrySet()) {
            System.out.println(entry.getKey() + "; Team Details: " + entry.getValue());
        }
        
        System.out.println("");
        System.out.println("*************************************************");
        
        System.out.println("Print the TreeMap:");
        System.out.println(treeMaps);
        
        System.out.println("*************************************************"); 
        
    }
}

