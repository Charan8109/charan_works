/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Estimate() {
    
   topoffw=10000;
   halfw=5000;
   minimumw=1000;
   raftw=150;
   vestw=300;
   max=3;
   min=1;
   dc9w=50000;
   md83w=75000;
  
    var weight=0,passweight,time;
   allDataEntered = true;
    
   if (document.getElementById("type").value === "dc9") {
     weight+=dc9w;
   } else if (document.getElementById("type").value === "md83") {
      weight+=md83w;
   } 

   if (document.getElementById("topoff").checked) {
      weight += topoffw;
   } else if (document.getElementById("half").checked) {
      weight += halfw;
   } else if (document.getElementById("minimum").checked) {
      weight += minimumw;
   }
   
   var numpass = parseInt(document.getElementById("numpass").value);
   
   
    if(isNaN(numpass)||numpass<0||numpass>100){
      allDataEntered = false;
      
      alert("You need to enter valid number of passengers from 0 to 100");
   }

   if (document.getElementById("liferaft").checked) {
      weight += raftw;
   }

   if (document.getElementById("lifevest").checked) {
      weight += vestw;
   }
   passweight= numpass * 150;
   weight += passweight;
   time= numpass * (Math.floor((Math.random() * max-min+1) + min));
   
   var msg1="Estimated weight of the aircraft is " + weight + " lb";
   var msg2="Estimated boarding time for the aircraft is " + time + " minutes";
   
   if(allDataEntered){
    document.getElementById("msg1").innerHTML = msg1;
    document.getElementById("msg2").innerHTML = msg2;  
   }
}