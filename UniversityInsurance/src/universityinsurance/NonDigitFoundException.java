/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

/**
 *
 * @author Saicharan Vaddadi
 */
public class NonDigitFoundException extends Exception {

    public NonDigitFoundException() {
    }

    
    public NonDigitFoundException(String msg) {
        super(msg);
    }
    
    
    
}
