/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import static universityinsurance.UniversityInsurance.ANNUAL_INSURANCE_AMOUNT;
import static universityinsurance.UniversityInsurance.INSURANCE_PERIOD;

/**
 *
 * @author Saicharan Vaddadi
 */
public abstract class AbstractEmployeeInsurance extends Employee implements UniversityInsurance {
    
    
    private int totalYearsOfPayment;

    public AbstractEmployeeInsurance(int age, String empId, String empName, int hiredYear, String mobileNumber,int totalYearsOfPayment) {
        super(age, empId, empName, hiredYear, mobileNumber);
        this.totalYearsOfPayment = totalYearsOfPayment;
    }

    public int getTotalYearsOfPayment() {        
        return totalYearsOfPayment;        
    }
    
    public abstract double calcDiscount(int age);
    
    @Override
    public double calcPremiumAmount(int totalYearsOfPayment,int age)throws IncorrectYearsException{
        double insuranceAmount=0.0;
        if(totalYearsOfPayment>INSURANCE_PERIOD||totalYearsOfPayment<=0){
            throw new IncorrectYearsException("IncorrectYearsException: The total number of years is either less than 0 or greater than 3");
        }
        
        if(totalYearsOfPayment==INSURANCE_PERIOD){
            insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment;
        }
        if(totalYearsOfPayment==2){
            insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment-((ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment*20)/100);
        }
        if(totalYearsOfPayment==1){
            insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment-((ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment*25)/100);
        }
        return insuranceAmount-calcDiscount(age);
    }

    
    
    @Override
    public String toString() {
        return super.toString();
    }    
}