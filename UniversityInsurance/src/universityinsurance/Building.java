/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.util.Objects;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Building implements Comparable<Building>{
    
    private String buildingAddress, buildingName, managerName, managerSSN;
    private int noOfFloors;

    public Building(String buildingAddress, String buildingName, String managerName, String managerSSN, int noOfFloors) {
        this.buildingAddress = buildingAddress;
        this.buildingName = buildingName;
        this.managerName = managerName;
        this.managerSSN = managerSSN;
        this.noOfFloors = noOfFloors;
    }

    public String getBuildingAddress() {
        return buildingAddress;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getManagerSSN() {
        return managerSSN;
    }

    public int getNoOfFloors() {
        return noOfFloors;
    }
    
     @Override
    public int compareTo(Building o) {
       return getBuildingName().compareTo(o.buildingName);
    }
    
    public boolean checkSSNNumber(String managerSSN)
                       throws NonDigitFoundException{
        for(int i=0;i<managerSSN.length();i++){
            if(managerSSN.charAt(i)<48||managerSSN.charAt(i)>57){
                throw new NonDigitFoundException("Manager's SSN is Not valid");
            }
        }
        return true;
    }
    
  

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Building other = (Building) obj;
        return Objects.equals(this.managerName, other.managerName);
    }

    @Override
    public String toString() {
        return String.format("%-15s %-15s %-2d %-15s %-15s",getBuildingAddress(),getBuildingName(),getNoOfFloors(),getManagerName(),getManagerSSN());
    }

   
  
}

    

