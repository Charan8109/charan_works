/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

/**
 *
 * @author Saicharan Vaddadi
 */
public class BuildingInsurance extends Building implements UniversityInsurance{
    private int totalYearsOfPayment;

    public BuildingInsurance(String buildingName, String buildingAddress, int noOfFloors, String managerName, String managerSSN, int totalYearsOfPayment) {
        
        super(buildingName, buildingAddress, managerName, managerSSN, noOfFloors);
        
        this.totalYearsOfPayment = totalYearsOfPayment;
    }
    
    public double calcExemption(int noOfFloors){
        double discount=0.0;
        if(noOfFloors<10){
            discount=(ANNUAL_INSURANCE_AMOUNT*20)/100;
        }
        if(noOfFloors>=10&&noOfFloors<=20){
            discount=(ANNUAL_INSURANCE_AMOUNT*10)/100;
        }
        if(noOfFloors>20&&noOfFloors<=30){
            discount=(ANNUAL_INSURANCE_AMOUNT*5)/100;
        }
        return discount;
    }

    public int getTotalYearsOfPayment() {
        return totalYearsOfPayment;
    }
    
    
    @Override
    public double calcPremiumAmount(int totalYearsOfPayment,
                                int noOfFloors)
                         throws IncorrectYearsException{
        double insuranceAmount=0.0;
        if(totalYearsOfPayment>INSURANCE_PERIOD||totalYearsOfPayment<0){
            throw new IncorrectYearsException("IncorrectYearsException: total years of repayment shouldn't be greater than the insurance duration or less than equal to zero.");
        }
        
        if(totalYearsOfPayment==INSURANCE_PERIOD){
            insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment;
        }
        if(totalYearsOfPayment==2){
            insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment-(ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment*25)/100;
        }
        if(totalYearsOfPayment==1){
           insuranceAmount=ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment-(ANNUAL_INSURANCE_AMOUNT*totalYearsOfPayment*27)/100;
        }
       return insuranceAmount-calcExemption(noOfFloors);
    }

    @Override
    public String toString() {
        return super.toString()+String.format("%-2d", totalYearsOfPayment);
    }
    
    
}
 
    
    


