/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author Saicharan Vaddadi
 */
public class Driver {

       public static void main(String[] args) throws FileNotFoundException {
        System.out.println("********* Employee Insurance Implementation*********");
        
        Scanner scanner=new Scanner(new File("employee.txt"));

        
        ArrayList<StaffInsurance> staffList=new ArrayList<>();
       ArrayList<FacultyInsurance> facultyList=new ArrayList<>();
       StaffInsurance staffInsurance;
       FacultyInsurance facultyInsurance;
        
        while(scanner.hasNext()){
        
            staffInsurance=new StaffInsurance(scanner.next(),scanner.next(),scanner.next(),
                            scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
            facultyInsurance=new FacultyInsurance(scanner.next(),scanner.next(),scanner.next(),
                            scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
            staffList.add(staffInsurance);
            facultyList.add(facultyInsurance); 
        }
        System.out.println("****************************************************");

        System.out.println("\nStaff Insurance List Of Elements: ORIGINAL LIST");
        for(StaffInsurance s1:staffList){
            System.out.println(s1);
        }
        
        System.out.println("****************************************************");
        System.out.println("\nFaculty Insurance List Of Elements: ORIGINAL LIST");
        for(FacultyInsurance f1:facultyList){
        System.out.println(f1);
        }

        System.out.println("****************************************************");

        System.out.println("\nStaff Insurance List Of Elements: NATURAL ORDER LIST"
                + " AS PER THE AGE");
        Collections.sort(staffList);
        for(StaffInsurance a:staffList){
            System.out.println(a);
        }
        System.out.println("****************************************************");
        
        System.out.println("\nFaculty Insurance List Of Elements: NATURAL ORDER "
                + "LIST AS PER THE NAME");
        Collections.sort(facultyList);
        for(FacultyInsurance a:facultyList){
            System.out.println(a);
        }
        System.out.println("****************************************************");

        System.out.println("\nStaffInsurance List Of Elements: Sort by Hiring Year of Staff Using Anonymous Class in Descending Order:");
        Collections.sort(staffList,new Comparator<StaffInsurance>(){
            @Override
            public int compare(StaffInsurance o1, StaffInsurance o2) {
                int a=0;
                if(o1.getHiredYear()>o2.getHiredYear()){
                    a=-1;
                }
                return a;
            }
            
        });
        for(StaffInsurance a:staffList){
            System.out.println(a);
        }
        
        FacultyHiredYearComparator f=new FacultyHiredYearComparator();
        System.out.println("****************************************************");
        Collections.sort(facultyList,f);
        System.out.println("FacultyInsurance List Of Elements: Sorted List natural Order Using separate class according to the Hiring year of the employee");
        
        for(FacultyInsurance a:facultyList){
            System.out.println(a);
        }

        System.out.println("****************************************************");
        StaffInsurance staffInsurance1 = new StaffInsurance("Sara", "666542111", "755695", 24, 2011, 1);
        
        StaffInsurance staffInsurance2=new StaffInsurance("Lindcy", "5634561234", "712345", 39, 2001, 6);
        
        StaffInsurance staffInsurance3=new StaffInsurance("Jack", "8163451234", "612398", 41, 2000, 3);
        System.out.println("");
        System.out.println("Checking Equals Method w.r.t employee name using staffInsurance1 and staffInsurance3 objects: "+staffInsurance1.equals(staffInsurance3));

        System.out.println("****************************************************");
        try{
            
            System.out.println("\nCalculating value calcPremiumAmount() method using staffInsurance2 object:");
            System.out.println(staffInsurance2.calcPremiumAmount(staffInsurance2.getTotalYearsOfPayment(), staffInsurance2.getAge()));
        }
        catch(IncorrectYearsException e){
            System.out.println(e.getMessage());
        }
        
        try{
            System.out.println("\nCalculating the value calcPremiumAmount() using staffInsurance3 object:");
            System.out.println("The value Of Insurance to be paid by a person if"
                    + " he/she pays the insurance within "+staffInsurance3.getTotalYearsOfPayment()
                    +" years is: $"+String.format("%.2f",staffInsurance3.calcPremiumAmount(staffInsurance3.getTotalYearsOfPayment(), staffInsurance3.getAge())));
        }
        catch(IncorrectYearsException e){
            System.out.println(e.getMessage());
        }
        
        System.out.println("****************************************************");

        try{
        System.out.println("Testing checkMobileNumber() method using 'staffInsurance2' object in Staff Insurance class: ");
            
            if(staffInsurance2.checkMobileNumber(staffInsurance2.getMobileNumber())){
                System.out.println("The mobile number is valid");
            }
        }
        catch(IncorrectAreaCodeException e){
            System.out.println(e.getMessage());
        }
        catch(IncorrectLengthException e){
            System.out.println("The mobile number is invalid");
        }
        catch(NonDigitFoundException e){
            System.out.println("NonDigitFoundException");
        }
        System.out.println("****************************************************");
        System.out.println("");
        System.out.println("********* Building Insurance Implementation*********");
        
        Scanner scanner2=new Scanner(new File("building.txt"));
       
        ArrayList<ResidentialInsurance> residentialList=new ArrayList<>();
        ArrayList<NonResidentialInsurance> nonResidentialList=new ArrayList<>();
        
        while(scanner2.hasNext()){
            NonResidentialInsurance nonResidentialInsurance=new 
                NonResidentialInsurance(scanner2.next(),scanner2.next(),scanner2.nextInt(),scanner2.next(),scanner2.next(),scanner2.nextInt());
            nonResidentialList.add(nonResidentialInsurance);
        
            ResidentialInsurance residentialInsurance= new
                ResidentialInsurance(scanner2.next(),scanner2.next(),scanner2.nextInt(),scanner2.next(),scanner2.next(),scanner2.nextInt());
            residentialList.add(residentialInsurance);
        }
        
        System.out.println("\nResidential Insurance List Of Elements: ORIGINAL LIST");
        for(ResidentialInsurance a:residentialList){
            System.out.println(a);
        }

        System.out.println("****************************************************");
        
        System.out.println("NonResidential Insurance List Of Elements: ORIGINAL LIST");
        for(NonResidentialInsurance a:nonResidentialList){
            System.out.println(a);
        }

        System.out.println("****************************************************");
        System.out.println("Residential Insurance List Of Elements: NATURAL ORDER LIST AS PER THE NUMBER OF FLOORS");
        Collections.sort(residentialList);
        for(ResidentialInsurance a:residentialList){
            System.out.println(a);
        }
        
        System.out.println("****************************************************");
        System.out.println("NonResidential Insurance List Of Elements: DESCENDING ORDER LIST AS PER THE NUMBER OF FLOORS");
        
        Collections.sort(nonResidentialList,new Comparator<NonResidentialInsurance>(){
            @Override
            public int compare(NonResidentialInsurance o1, NonResidentialInsurance o2) {
                int a=0;
                if(o1.getNoOfFloors()>o2.getNoOfFloors()){
                    a=-1;
                }
                return a;
            }
        });
        for(NonResidentialInsurance a:nonResidentialList){
            System.out.println(a);
        }
      
        System.out.println("****************************************************");
        System.out.println("Residential Insurance List Of Elements: Sort by Total Years Of Payment Using Anonymous Class in Descending Order:");
        Collections.sort(residentialList,new Comparator<ResidentialInsurance>(){
            @Override
            public int compare(ResidentialInsurance o1, ResidentialInsurance o2) {
                int a=0;
                if(o1.getTotalYearsOfPayment()>o2.getTotalYearsOfPayment()){
                    a=-1;
                }
                return a;
            }
        });
        for(ResidentialInsurance a:residentialList){
            System.out.println(a);
        }
        
        System.out.println("****************************************************");
        System.out.println("NonResidential Insurance List Of Elements: Sort by Manager's SSN Using Anonymous Class in Ascending Order:");
        Collections.sort(nonResidentialList,new Comparator<NonResidentialInsurance>(){
            @Override
            public int compare(NonResidentialInsurance o1, NonResidentialInsurance o2) {
                     return o1.getManagerSSN().compareTo(o2.getManagerSSN());
            }
        });
        
        for(NonResidentialInsurance a:nonResidentialList){
            System.out.println(a);
        }
        
        System.out.println("****************************************************");
        
        System.out.println("Testing checkSSNNumber() method in second index of residentialList: ");
        try{
            if(residentialList.get(2).checkSSNNumber(residentialList.get(2).getManagerSSN()));
            System.out.println("Manager's SSN is valid");
        }
        catch(NonDigitFoundException e){
            System.out.println(e.getMessage());
        }
        
        System.out.println("****************************************************");
        System.out.println("Checking Equals Method w.r.t manager name Using "
                + "residentialInsurance objects in residentialList at index 1 "
                + "and 2: "+residentialList.get(1).getManagerName().equals(residentialList.get(2).getManagerName()));

        System.out.println("****************************************************");
        System.out.println("Checking Equals Method w.r.t manager's SSN Using "
                + "nonresidentialInsurance objects in nonResidentialList at "
                + "index 1 and 2: "+nonResidentialList.get(1).getManagerSSN().equals(nonResidentialList.get(2).getManagerSSN()));
        System.out.println("****************************************************");
        
        System.out.println("Calculating calcPremiumAmount() method using residentialInsurance object in 0th index of residentialList:");
        try{
        System.out.println("The value Of Insurance to be paid by a person if "
                + "he/she pays the insurance within "+residentialList.get(0).getTotalYearsOfPayment()+" years is : $ "
                +String.format("%.2f",residentialList.get(0).calcPremiumAmount(residentialList.get(0).getTotalYearsOfPayment(),residentialList.get(0).getNoOfFloors())));
        }
        catch(IncorrectYearsException e){
            System.out.println(e.getMessage());
        }
       
        System.out.println("****************************************************");
        System.out.println("Calculating calcPremiumAmount() method using nonResidentialInsurance object present at index 2 of nonResidentialList:");
        try{
        System.out.println(nonResidentialList.get(0).calcPremiumAmount(nonResidentialList.get(2).getTotalYearsOfPayment(),nonResidentialList.get(0).getNoOfFloors()));
        }
        catch(IncorrectYearsException e){
            System.out.println(e.getMessage());
        }
        System.out.println("****************************************************");

    }   
    
}
