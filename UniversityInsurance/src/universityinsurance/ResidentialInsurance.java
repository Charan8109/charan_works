/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

/**
 *
 * @author Saicharan Vaddadi
 */
public class ResidentialInsurance extends BuildingInsurance{

    public ResidentialInsurance(String buildingName, String buildingAddress, int totalYearsOfPayment, String managerName, String managerSSN, int noOfFloors) {
        super(buildingName, buildingAddress, totalYearsOfPayment, managerName, managerSSN,noOfFloors );
    }
    
    @Override
    public double calcExemption(int noOfFloors){
       double discount=0.0;
        if(noOfFloors<10){
            discount=(ANNUAL_INSURANCE_AMOUNT*25)/100;
        }
        if(noOfFloors>=10&&noOfFloors<=20){
            discount=(ANNUAL_INSURANCE_AMOUNT*15)/100;
        }
        if(noOfFloors>20&&noOfFloors<=30){
            discount=(ANNUAL_INSURANCE_AMOUNT*10)/100;
        }
        return discount; 
    }
    
    @Override
    public int compareTo(Building building){
        return Integer.compare(getNoOfFloors(), building.getNoOfFloors());
    }
    
    
    
    @Override
    public String toString(){
        return super.toString();
        
        
    }
}

    


