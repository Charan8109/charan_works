/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.util.Comparator;

/**
 *
 * @author Saicharan Vaddadi
 */
public class FacultyHiredYearComparator implements Comparator<FacultyInsurance>{

    public FacultyHiredYearComparator() {
    }
    
    
    @Override
    public int compare(FacultyInsurance f1,FacultyInsurance f2){
        return Integer.compare(f1.getHiredYear(),f2.getHiredYear());
    }
}

