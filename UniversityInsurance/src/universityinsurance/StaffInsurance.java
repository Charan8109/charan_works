/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.util.Objects;
import static universityinsurance.UniversityInsurance.ANNUAL_INSURANCE_AMOUNT;

/**
 *
 * @author Saicharan Vaddadi
 */
public class StaffInsurance  extends AbstractEmployeeInsurance{

    public StaffInsurance(String empName,String mobileNumber,String empId, int age, int hiredYear, int totalYearsOfPayment) {
        super(age, empId, empName, hiredYear, mobileNumber,totalYearsOfPayment);
    }
    
    @Override
    public double calcDiscount(int age){
        double discount=0.0;
        if(age<20){
            discount=(ANNUAL_INSURANCE_AMOUNT*2)/100;
        }
        if(age>=20&&age<=40){
            discount=(ANNUAL_INSURANCE_AMOUNT*1)/100;
        }
        return discount;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if(!Objects.equals(getEmpName(), other.getEmpName())) {
            return false;
        }
        return true;
    }
}

    

