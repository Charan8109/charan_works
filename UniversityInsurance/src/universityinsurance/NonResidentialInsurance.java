/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.util.Objects;

/**
 *
 * @author Saicharan Vaddadi
 */
public class NonResidentialInsurance extends BuildingInsurance{

    public NonResidentialInsurance(String buildingName, String buildingAddress, int totalYearsOfPayment,  String managerName, String managerSSN, int noOfFloors) {
        super(buildingName, buildingAddress, totalYearsOfPayment, managerName, managerSSN ,noOfFloors);
    }
    
    @Override
    public int compareTo(Building building){
        return Integer.compare(getNoOfFloors(),building.getNoOfFloors());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Building other = (Building) obj;
        if (!Objects.equals(this.getManagerSSN(), other.getManagerSSN())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        
        hash = 97 * hash + Objects.hashCode(this.getManagerSSN());
        return hash;
    }
    
    @Override
    public String toString(){
        return super.toString();
    }
    
    
}

    


