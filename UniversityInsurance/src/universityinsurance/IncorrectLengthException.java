/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

/**
 *
 * @author Saicharan Vaddadi
 */
public class IncorrectLengthException extends Exception {

    
    public IncorrectLengthException() {
    }

    
    
    public IncorrectLengthException(String msg) {
        super(msg);
    }
}


