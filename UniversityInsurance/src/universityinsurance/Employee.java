/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universityinsurance;

import java.util.Objects;



/**
 *
 * @author Saicharan Vaddadi
 */
public class Employee implements Comparable<Employee>{
    private int age,hiredYear;
    private String empId, empName,  mobileNumber;

    public Employee(int age, String empId, String empName, int hiredYear, String mobileNumber) {
        this.age = age;
        this.empId = empId;
        this.empName = empName;
        this.hiredYear = hiredYear;
        this.mobileNumber = mobileNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getHiredYear() {
        return hiredYear;
    }

    public void setHiredYear(int hiredYear) {
        this.hiredYear = hiredYear;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public boolean checkMobileNumber(String mobileNumber)
                          throws IncorrectAreaCodeException,
                                 IncorrectLengthException,
                                 NonDigitFoundException{
        if(mobileNumber.length()!=10){
            throw new IncorrectLengthException("Number of digits must be 10");
        }
        String areaCode=mobileNumber.substring(0, 3);
        if(areaCode.equals("660")||areaCode.equals(224)){
            
        }
        else{
            throw new IncorrectAreaCodeException("IncorrectAreaCodeException: The area code is invalid");
        }
        if(!mobileNumber.matches("[0-9]")){
//        for(int i=0;i<mobileNumber.length();i++){
//            if(mobileNumber.charAt(i)!='0'|| mobileNumber.charAt(i)!='1'||
//                    mobileNumber.charAt(i)!='2'||mobileNumber.charAt(i)!='3'||
//                    mobileNumber.charAt(i)!='4'||mobileNumber.charAt(i)!='5'||
//                    mobileNumber.charAt(i)!='6'||mobileNumber.charAt(i)!='7'||
//                    mobileNumber.charAt(i)!='8'||mobileNumber.charAt(i)!='9'){
                throw new NonDigitFoundException("Mobile should consist of only digits 0-9");
                
            }
        
        return true;
    }

    @Override
    public int compareTo(Employee o) {
        return Integer.compare(getAge(), o.age);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.empId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        return Objects.equals(this.empId, other.empId);
    }

    @Override
    public String toString() {
        return String.format("%-10s %-10s %-10s%3d %3d ",getEmpName(),getMobileNumber(),getEmpId(),getAge(),getHiredYear());
    }
    
    
}

    

