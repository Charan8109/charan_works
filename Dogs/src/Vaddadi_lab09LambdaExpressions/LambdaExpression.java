/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vaddadi_lab09LambdaExpressions;

import java.util.function.BiFunction;
import java.util.function.IntFunction;

/**
 *
 * @author Saicharan Vaddadi
 */
public class LambdaExpression {

  
    public static void main(String[] args) {
        
        
        IntFunction incrementBy2 = x -> x + 2;
        System.out.println(incrementBy2.apply(12));
        System.out.println();
        
        
        
        //first apply is executed (x -> x);
        //then increment operator is executed
        IntFunction incrementBy1Version1 = x -> x++;
        System.out.println(incrementBy1Version1.apply(12));
        System.out.println();
        
        
        
        // first increment operator is applied
        // then apply is executed
        IntFunction incrementBy1Version2 = x -> ++x;
        System.out.println(incrementBy1Version2.apply(12));
        System.out.println();
        
        
        
        
        IntFunction threeOrFour = x -> {
            switch (x) {
                case 3:
                    return "three";
                case 4:
                    return "four";
                default:
                    return "other";
            }
        };

        System.out.println(threeOrFour.apply(4));
        System.out.println(threeOrFour.apply(1));
        System.out.println(threeOrFour.apply(3));
        System.out.println();
        
        
        
        IntFunction zeroOrOne = x -> x == 0 ? "zero" : x == 1 ? "one" : "other";
        System.out.println(zeroOrOne.apply(0));
        System.out.println(zeroOrOne.apply(1));
        System.out.println(zeroOrOne.apply(12));
        System.out.println();
        
        
        


        BiFunction<Integer, Integer, Integer> myFunction = (a, b) -> a - b;
        System.out.println(myFunction.apply(30, 100));
    }

}